#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // ==
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Voting.h"

// g++-4.7 -fprofile-arcs -ftest-coverage -pedantic -std=c++11 -Wall Voting.c++ TestVoting.c++ -o TestVoting -lgtest -lgtest_main -lpthread

//gcov-4.7 -b Voting.c++ TestVoting.c++

// ---
// read_args
// ---

TEST(Voting, read_args_1){
  std::istringstream r("1\n\n2\nChad Custodio\nAndrew Vohl\n1 2\n2 1\n2 1");
  int v;
  voting_read_args(r, v);
  ASSERT_EQ(1, v);
}

TEST(Voting, read_args_2){
  std::istringstream r("2\n\n2\nChad\nAndrew\n1 2\n2 1\n2 1\n\n2\nChad\nAndrew\n2 1\n1 2\n1 2");
  int v;
  voting_read_args(r, v);
  ASSERT_EQ(2, v);
}

TEST(Voting, read_args_3){
  std::istringstream r("1\n\n3\nChad\nAndrew\nGabe\n1 2 3\n2 1 3\n2 3 1");
  int v;
  voting_read_args(r, v);
  ASSERT_EQ(1, v);
}

// ---
// read_cand
// ---

TEST(Voting, read_cand_1){
  std::istringstream r("3\nChad\nAndrew\nGabe");
  int v;
  voting_read_cand(r, v);
  ASSERT_EQ(3, v);
}

TEST(Voting, read_cand_2){
  std::istringstream r("20\nChad\nAndrew\nGabe\nVohl\nCustodio\nThien\nVo\nKeith\nGeorge\nJungle\nJon\nTron\nAustin\nDallas\nHouston\nGarland\nTexas\nChuck\nYvonne\nNick");
  int v;
  voting_read_cand(r, v);
  ASSERT_EQ(20, v);
}

TEST(Voting, read_cand_3){
  std::istringstream r("2\nChad\nAndrew");
  int v;
  voting_read_cand(r, v);
  ASSERT_EQ(2, v);
}

// ---
// read_ballot
// ---

TEST(Voting, read_ballot_1){
  std::istringstream r("1 2 3\n2 1 3\n2 3 1");
  bool v = voting_read_ballot(r);
  ASSERT_EQ(1, v);
}

TEST(Voting, read_ballot_2){
  std::istringstream r("");
  bool v = voting_read_ballot(r);
  ASSERT_EQ(0, v);
}

TEST(Voting, read_ballot_3){
  std::istringstream r("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20\n");
  bool v = voting_read_ballot(r);
  ASSERT_EQ(1, v);
}

// ---
// eval
// ---

TEST(Voting, eval_1){
  std::istringstream r("1\n\n2\nChad\nAndrew");
  int v;
  int j;
  int votes = 0;
  voting_read_args(r, v);
  voting_read_cand(r, j);
  while(voting_read_ballot(r)){
    ++votes;
  }
  std::string s = voting_eval(votes, j);
  ASSERT_EQ("Chad\nAndrew", s);
}

TEST(Voting, eval_2){
  std::istringstream r("1\n\n3\nChad\nAndrew\nGabe\n1 2 3\n1 3 2\n2 1 3\n2 3 1\n3 1 2");
  int v;
  int j;
  int votes = 0;
  voting_read_args(r, v);
  voting_read_cand(r, j);
  while(voting_read_ballot(r)){
    ++votes;
  }
  std::string s = voting_eval(votes, j);
  ASSERT_EQ("Chad", s);
}

TEST(Voting, eval_3){
  std::istringstream r("1\n\n3\nAndrew\nChad\nGabe\n1 2 3\n2 3 1\n1 3 2");
  int v;
  int j;
  int votes = 0;
  voting_read_args(r, v);
  voting_read_cand(r, j);
  while(voting_read_ballot(r)){
    ++votes;
  }
  std::string s = voting_eval(votes, j);
  ASSERT_EQ("Andrew", s);
}

// ---
// solve
// ---

TEST(Voting, solve_1){
  std::istringstream r("1\n\n2\nAndrew\nChad");
  std::ostringstream w;
  voting_solve(r, w);
  ASSERT_EQ("Andrew\nChad\n", w.str());
}

TEST(Voting, solve_2){
  std::istringstream r("2\n\n2\nAndrew\nChad\n1 2\n2 1\n2 1\n\n2\nAndrew\nChad\n1 2\n1 2\n2 1");
  std::ostringstream w;
  voting_solve(r, w);
  ASSERT_EQ("Chad\n\nAndrew\n", w.str());
}

TEST(Voting, solve_3){
  std::istringstream r("1\n\n2\nAndrew\nChad\n1 2\n1 2\n2 1\n1 2");
  std::ostringstream w;
  voting_solve(r, w);
  ASSERT_EQ("Andrew\n", w.str());
}

