#ifndef Voting_h
#define Voting_h

// --------
// includes
// --------

#include <iostream> 
#include <string>

void voting_read_args (std::istream&, int&);

void voting_read_cand (std::istream&, int&);

bool voting_read_ballot (std::istream&);

std::string voting_eval (int&, int&);

void voting_solve(std::istream&, std::ostream&);
#endif
