#ifdef ONLINE_JUDGE
    #define NDEBUG
#endif

//--------
//Includes
//--------

#include <cassert>  
#include <iostream> 
#include <sstream>
#include <string>
#include <map>

//----------------
//Global_Variables
//----------------

using namespace std;
/*array of names, (index + 1) represents the interger representation of the name on the voting ballot*/
string name[20];
/*array that holds the count of votes per person, (index + 1) is integer representation of the name*/
int votes [20];
/*Map's remainder of voting card to a name */
//cache
multimap<int, string> ballot_list;

//---------------
//votin_read_args
//---------------

void voting_read_args(istream& r, int& i){
  string s;

  getline(r, s);
  stringstream ss(s);
  ss >> i;
  getline(r, s);
  assert(i > 0); //Number of cases have to be greater than 0
}

//----------------
//voting_read_cand
//----------------

void voting_read_cand (istream& r, int& j){
  int i = 0;
  string s;

  getline(r, s);
  stringstream ss(s);
  ss >> j;

  while(i < j){
    getline(r, s);
    name[i] = s;
    ++i;
  }
  //boundary cases for number of candidates
  assert(j > 0);
  assert(j < 21);
}

//------------------
//voting_read_ballot
//------------------

bool voting_read_ballot(istream& r){
  int i;
  string s;

  getline(r, s);
  stringstream ss(s);

  if(s == ""){
    return false;
  }
  else{
    ss >> i;
    getline (ss, s);
    ballot_list.insert(pair<int, string>(i, s));
    ++votes[i-1];
    return true;
  }
}

//-----------
//voting_eval
//-----------

string voting_eval(int& numV, int& cand){
  int min = 1001;
  int max = 0;
  int count = 0;
  string s = "";
  int i;

  if(numV == 0){
    for(int i = 0; i < cand; ++i){
      if(count > 0){
        s += "\n" + name[i];
      }
      else{
        s += name[i];
      }
      ++count;
    }
    return s;
  }

  while(1){
    //finding the min and max in an array
    for(int elem = 0; elem < cand; ++elem){
      //auto win if this case is seen
      if(votes[elem] > (numV/2)){
        return name[elem];
      }
      else{
        if(votes[elem] < min && votes[elem] > 0){
          min = votes[elem];
        }
        if(votes[elem] > max){
          max = votes[elem];
        }
      }
    }

    if(max == min){
      //We have a tie for winner; multimap contains keys of corresponding to winners
      //very bad solution
      s = "";
      for(int win = 0; win < cand; ++win){
        if(votes[win] > 0){
          if(count > 0){
            s += "\n" + name[win];
          }
          else{
            s += name[win];
          }
          ++count;
        }
      }
      return s;
    }
    //Fucking Brilliant code
    for(int elem = 0; elem < cand; ++elem){
      if(min == votes[elem]){
        multimap<int, string>::iterator it;
        for(it = ballot_list.equal_range(elem + 1).first; it != ballot_list.equal_range(elem + 1).second; ++it){
          s = (*it).second;
          stringstream ss(s);
          ss >> i;
          while(votes[i - 1] <= min){
            ss >> i;
          }
          getline(ss, s);
          ballot_list.insert(pair<int, string>(i, s));
          ++votes[i - 1];
        }
        ballot_list.erase(elem + 1);
        votes[elem] = 0;
      }
    }
  min = 1001;
  }
}

//------------
//voting_solve
//------------

void voting_solve(istream& r, ostream& w){
  //Number of different elections in input
  int cases;
  //NUmber of Candidates in an election
  int numCand;
  //Number of voter cards/ballots
  int totalVotes;

  voting_read_args(r, cases);

  while(cases > 0){
    voting_read_cand(r, numCand);
    totalVotes = 0;
    while(voting_read_ballot(r)){
      ++totalVotes;
    }

    if(cases == 1){
      w << voting_eval(totalVotes, numCand) << endl;
    }
    else{
      w << voting_eval(totalVotes, numCand) << endl << endl;
    }
    //reset
    for(int i = 0; i < 20; ++i){
      votes[i] = 0;
      name[i] = "";
    }

    ballot_list.clear();

    --cases;
    //Can't have more than 1000 voter cards
    assert(totalVotes < 1001);
  }
}

int main () {
    using namespace std;
    voting_solve(cin, cout);
    return 0;
}